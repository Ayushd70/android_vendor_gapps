# Accessibility
-product/app/talkback/talkback.apk;PRESIGNED

# Android Auto
-product/priv-app/AndroidAutoStubPrebuilt/AndroidAutoStubPrebuilt.apk;PRESIGNED

# Browser
-product/app/Chrome-Stub/Chrome-Stub.apk;PRESIGNED
-product/app/TrichromeLibrary-Stub/TrichromeLibrary-Stub.apk;PRESIGNED
-product/app/WebViewGoogle-Stub/WebViewGoogle-Stub.apk;PRESIGNED
product/app/Chrome/Chrome.apk.gz
product/app/TrichromeLibrary/TrichromeLibrary.apk.gz
product/app/WebViewGoogle/WebViewGoogle.apk.gz

# Calculator
-product/app/CalculatorGooglePrebuilt/CalculatorGooglePrebuilt.apk;PRESIGNED

# Calendar
-product/app/CalendarGooglePrebuilt/CalendarGooglePrebuilt.apk;PRESIGNED

# Camera
-product/app/arcore/arcore.apk;PRESIGNED
-product/app/GoogleCamera/GoogleCamera.apk;PRESIGNED
-product/app/Ornament/Ornament.apk;PRESIGNED

# Clock
-product/app/PrebuiltDeskClockGoogle/PrebuiltDeskClockGoogle.apk;PRESIGNED

# Configs
product/etc/preferred-apps/google.xml
product/etc/sysconfig/google_build.xml
product/etc/sysconfig/google-hiddenapi-package-whitelist.xml
product/etc/sysconfig/google.xml
product/etc/sysconfig/nexus.xml
product/etc/sysconfig/nga.xml
product/etc/sysconfig/pixel_experience_2017.xml
product/etc/sysconfig/pixel_experience_2018.xml
product/etc/sysconfig/pixel_experience_2019_midyear.xml
product/etc/sysconfig/pixel_experience_2019.xml
product/etc/sysconfig/pixel_experience_2020_midyear.xml
product/etc/sysconfig/pixel_experience_2020.xml

# Connectivity
-product/priv-app/ConnMetrics/ConnMetrics.apk;PRESIGNED

# Device Health Services
-product/priv-app/TurboPrebuilt/TurboPrebuilt.apk;PRESIGNED

# Gboard
-product/app/LatinIMEGooglePrebuilt/LatinIMEGooglePrebuilt.apk;PRESIGNED

# Google Search
-product/priv-app/Velvet/Velvet.apk;PRESIGNED

# Gmail
-product/app/PrebuiltGmail/PrebuiltGmail.apk;PRESIGNED

# Google Play
-app/GoogleExtShared/GoogleExtShared.apk;PRESIGNED
-product/priv-app/ConfigUpdater/ConfigUpdater.apk;PRESIGNED
-product/priv-app/Phonesky/Phonesky.apk;PRESIGNED
-product/priv-app/PrebuiltGmsCore/m/independent/AndroidPlatformServices.apk;PRESIGNED
-product/priv-app/PrebuiltGmsCore/PrebuiltGmsCoreRvc.apk;PRESIGNED
-product/priv-app/PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreRvc_AdsDynamite.apk;PRESIGNED
-product/priv-app/PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreRvc_CronetDynamite.apk;PRESIGNED
-product/priv-app/PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreRvc_DynamiteLoader.apk;PRESIGNED
-product/priv-app/PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreRvc_DynamiteModulesA.apk;PRESIGNED
-product/priv-app/PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreRvc_DynamiteModulesC.apk;PRESIGNED
-product/priv-app/PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreRvc_GoogleCertificates.apk;PRESIGNED
-product/priv-app/PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreRvc_MapsDynamite.apk;PRESIGNED
-product/priv-app/PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreRvc_MeasurementDynamite.apk;PRESIGNED
-system_ext/priv-app/GoogleFeedback/GoogleFeedback.apk;PRESIGNED
-system_ext/priv-app/GoogleServicesFramework/GoogleServicesFramework.apk;PRESIGNED

# Live Wallpaper
-product/app/WallpapersBReel2020/WallpapersBReel2020.apk;PRESIGNED
product/lib64/libgdx.so
-product/priv-app/PixelLiveWallpaperPrebuilt/PixelLiveWallpaperPrebuilt.apk;PRESIGNED

# Location
-product/app/LocationHistoryPrebuilt/LocationHistoryPrebuilt.apk;PRESIGNED

# Permissions
etc/permissions/privapp-permissions-google.xml
product/etc/default-permissions/default-permissions.xml
product/etc/permissions/com.android.omadm.service.xml
product/etc/permissions/com.google.android.dialer.support.xml
product/etc/permissions/com.google.omadm.trigger.xml
product/etc/permissions/split-permissions-google.xml
product/etc/permissions/privapp-permissions-pixel.xml|99aadec879455617798c2631070892bfed1ddd65
product/etc/permissions/privapp-permissions-google-p.xml
system_ext/etc/permissions/privapp-permissions-google-se.xml

# Personalization
-product/priv-app/DevicePersonalizationPrebuiltPixel2020/DevicePersonalizationPrebuiltPixel2020.apk;PRESIGNED

# Phone
-product/app/GoogleContacts/GoogleContacts.apk;PRESIGNED
-product/app/GoogleContactsSyncAdapter/GoogleContactsSyncAdapter.apk;PRESIGNED
-product/app/PrebuiltBugle/PrebuiltBugle.apk;PRESIGNED
-product/priv-app/GoogleDialer/GoogleDialer.apk;PRESIGNED
-product/framework/com.google.android.dialer.support.jar;PRESIGNED

# Photo/Video
-product/app/MarkupGoogle/MarkupGoogle.apk;PRESIGNED
-product/app/Photos/Photos.apk;PRESIGNED
product/lib64/libsketchology_native.so

<<<<<<< HEAD
=======
# Pixel Launcher
-system_ext/priv-app/NexusLauncherRelease/NexusLauncherRelease.apk;PRESIGNED
system_ext/etc/permissions/com.android.launcher3.xml

>>>>>>> cc36b99... update to redfin
# Print
-app/GooglePrintRecommendationService/GooglePrintRecommendationService.apk;PRESIGNED

# Setup
-product/priv-app/AndroidMigratePrebuilt/AndroidMigratePrebuilt.apk;PRESIGNED
-product/priv-app/OTAConfigPrebuilt/OTAConfigPrebuilt.apk;PRESIGNED
-product/priv-app/PartnerSetupPrebuilt/PartnerSetupPrebuilt.apk;PRESIGNED
-product/priv-app/SetupWizardPrebuilt/SetupWizardPrebuilt.apk;PRESIGNED
-system_ext/priv-app/GoogleOneTimeInitializer/GoogleOneTimeInitializer.apk;PRESIGNED
-system_ext/priv-app/PixelSetupWizard/PixelSetupWizard.apk;PRESIGNED

# Text-to-Speech
-product/app/GoogleTTS/GoogleTTS.apk;PRESIGNED

# Wallpaper
-product/app/WallpapersBReel2019/WallpapersBReel2019.apk;PRESIGNED
product/lib64/libgdx.so
-product/priv-app/PixelLiveWallpaperPrebuilt/PixelLiveWallpaperPrebuilt.apk;PRESIGNED
-system_ext/priv-app/WallpaperPickerGooglePrebuilt/WallpaperPickerGooglePrebuilt.apk;PRESIGNED

# Wellbeing
-product/priv-app/WellbeingPrebuilt/WellbeingPrebuilt.apk;PRESIGNED
